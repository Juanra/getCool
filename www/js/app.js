// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', [
  'ionic',
  'ngMaterial',
  'ngCordova',
  'controllers',
  'services'])

.run(function($ionicPlatform,$cordovaCamera,$window) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);

      var tapEnabled = false; //enable tap take picture
      var dragEnabled = true; //enable preview box drag across the screen
      var toBack = true; //send preview box to the back of the webview
      var rect = {x: 0, y: 0, width: 10, height:10};
      var widthScreen = $window.innerWidth;
      var heightScreen = $window.innerHeight;
      cordova.plugins.camerapreview.startCamera({x:0,y:0,width:screen.width,height:screen.height+50}, "front", tapEnabled, dragEnabled, toBack);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})
.config(function(
  $stateProvider,
  $urlRouterProvider,
  $ionicConfigProvider){
    $ionicConfigProvider.views.maxCache(0);
    $stateProvider
    .state('landing', {
      url: '/landing',
      templateUrl: 'templates/landing.html',
      controller: 'LandingCtrl'
    });

    $urlRouterProvider.otherwise('/landing');
  });
