angular.module('landingCtrl', [

])
.controller('LandingCtrl', function($scope,$state,$cordovaCamera) {

  $scope.goCamera = function (){
    console.log("hola");
    var options = {
      quality: 75,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.CAMERA,
      allowEdit: true,
      encodingType: Camera.EncodingType.JPEG,
      targetWidth: 300,
      targetHeight: 300,
      popoverOptions: CameraPopoverOptions,
      saveToPhotoAlbum: false
    };
    $cordovaCamera.getPicture(options).then(function(imgData) {
      console.log("good");
    },function (err) {
      console.log("Error");
    });
  }
});
